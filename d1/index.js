/*
	we use "require" directive to load Node.js modules
	module is a software component or a part of a program that contains one or more routines

	"http" lets Node.js transfer data usring Hyper Text Transfer Protocol

	"http" module is a set of individual files that contain a code to create a "component" that helps establish data transfer between applications

	HTTP is a protocol that allows the fetching of resources such as HTML documents

	Clients(browser) and server(Node.js/expressJS applications) communicate by exchanging individual messages

	message that came from the client - request

	message that came from the server - response 
*/

let http = require("http");

/*
	http - we are now trying to use the http module for us to create the server side-application

	createServer() - found inside the module; a method that accepts a function as its argument for a creation of a server
	(request, response) - arguments that are passed to the createServer method; this would allow us to receive request(1st parameter) and send responses(2nd parameter)
*/


http.createServer(function(request, response){
	// writeHead()
		// set a status code for the response - 200 means OK status
		// set Content-Type; using "text/plain" means that we are sending plain text as a response
	response.writeHead(200, {"Content-Type": "text/plain"});

	// we use response.end to denote the last stage of the communication which is sending of the response from the server
	// the code below will send the response with text content "Hello World"
	response.end("Hello World");


// .listen() allows our application to be run in our local devices through a specified port 
/*
	port - virtual point where network connections start and end
	each port is associated with a specific process/service
*/
// the code belower means that the server will be assigned to port 4000 via .listen(4000) method where the server will listen to any request that are sent to it eventually communicating with our server
}).listen(4000);

/*
	use node index.js to run the server
	press ctrl + c to terminante the gitbash process
*/

// used to confirm is running on a port
console.log("Server running at port 4000");

/*
	nodemon
		-installing this package will allow the server to automatically restart when files have been changed for update i.e. saving the files
		nmp install -g nodemon
		-npm install means that we are going to acess the nmp and install one of the packages that in its library

		-"-g" means that we are going to install the package globally. this means that even if we are on the other directories/repositories in our device, we could still use nodemon

		-"nodemond"- is the package we are going to install
		
*/






